﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Core.Global
{
	public class ObjectPoolSystem // === ПУЛ ОБЪЕКТОВ === //
	{
		[System.NonSerialized] public GameObject pool = null;
		[System.NonSerialized] public GameObject prefab = null;
		List<GameObject> items = new List<GameObject> ();

		public void ReturnItem(GameObject it) // ====================== Возвращаем конкретный объект в ПУЛ
		{
			foreach (GameObject item in items) if (item == it)
			{
				if (item.transform.parent != pool.transform)
				{
					item.transform.parent = pool.transform;
					item.SetActive(false);
				}
				return;
			}
		}

		public void ReturnAllItems() // ====================== Возвращаем все объекты в пул
		{
			foreach (GameObject item in items)
			{
				if (item.transform.parent != pool.transform)
				{
					item.transform.parent = pool.transform;
					item.SetActive(false);
				}
			}
		}
		
		public GameObject InitObject(GameObject newParentObj) // ====================== Инициируем объект
		{
			foreach (GameObject item in items) // === Ищем свободный объект в пуле
			{
				if (item.transform.parent == pool.transform)
				{
					item.SetActive(true);
					item.transform.parent = newParentObj.transform;
					return item;
				}
			}
			
			GameObject newItem = NGUITools.AddChild (newParentObj,prefab);  // === Если не нашли - создаём новый
			items.Add (newItem);
			newItem.SetActive(true);
			newItem.transform.parent = newParentObj.transform;
			return newItem;
		}

		public OCore_ObjectPool (GameObject setPool,GameObject setPrefab) // ====================== Инициируем пул
		{
			pool = setPool;
			prefab = setPrefab;
		}
	}
}