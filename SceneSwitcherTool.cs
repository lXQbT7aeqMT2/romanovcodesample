﻿using UnityEngine;
using System.Collections;
using Core.Global;

namespace Tools
{
	public class SceneSwitcherTool : MonoBehaviour // === СИСТЕМА ПЕРЕКЛЮЧЕНИЯ МЕЖДУ РАЗНЫМИ ОКНАМИ В РАМКАХ ОДНОЙ СЦЕНЫ Unity3D === //
	{
		[System.NonSerialized] public UIPanel pnlCurrentScene;
		[System.NonSerialized] public UIPanel pnlNextScene;
		public int sceneDepth = 5;
		public bool isFastSwitcher = false;
		bool isForward = true;

		float progressValue = 0f;
		float progress
		{
			get
			{
				return progressValue;
			}
			set
			{
				if (progressValue != value)
				{
					progressValue = value;

					if (pnlCurrentScene != null && pnlNextScene != null)
					{
						if (progressValue >= 1f)
						{
							progressValue = 1f;
							SetPanelNull (pnlCurrentScene, false);
							pnlCurrentScene = pnlNextScene;

							pnlCurrentScene.alpha = 1f;
							pnlCurrentScene.transform.localScale = Vector3.one;
							pnlCurrentScene.depth = sceneDepth;
						}
						else
						{
							float v = GCore_CurveCollection.inst.SemiWave.Evaluate(progressValue);
							if (isForward)
							{
								pnlCurrentScene.transform.localScale = Vector3.one * (1f + v);
								pnlNextScene.transform.localScale = Vector3.one * (v);
							}
							else
							{
								pnlCurrentScene.transform.localScale = Vector3.one * (1f - v);
								pnlNextScene.transform.localScale = Vector3.one * (2f - v);
							}
							pnlCurrentScene.alpha = 1f - v;
							pnlNextScene.alpha = v;
						}
					}
				}
			}
		}

		public bool isReady
		{
			get
			{
				return pnlCurrentScene == pnlNextScene;
			}
		}


		public void SwitchScene(UIPanel newScene, bool Forward)
		{
			if (pnlCurrentScene == newScene) return;
			isForward = Forward;
			progressValue = 0f;
			pnlNextScene = newScene;
			SetPanelNull (pnlNextScene, true);

			if (! isFastSwitcher) GameCore.blockSceneSwitch = 0.33f;
		}
		
		public void InitScene(UIPanel newScene)
		{
			if (pnlCurrentScene == newScene) return;
			progressValue = 0f;
			if (pnlCurrentScene != null) SetPanelNull(pnlCurrentScene,false);
			if (pnlNextScene != null) SetPanelNull(pnlNextScene,false);
			pnlCurrentScene = pnlNextScene = newScene;
			SetPanelShowed (pnlCurrentScene);
		}

		void Update()
		{
			if (pnlCurrentScene != pnlNextScene && pnlNextScene != null)
			{
				if (isFastSwitcher) progress += Time.deltaTime*5f;
				else progress += Time.deltaTime*3f;
			}
		}

		void SetPanelNull(UIPanel pan, bool isActive)
		{
			if (isForward)
			{
				pan.depth = sceneDepth - 1;
				pan.transform.localScale = Vector3.zero;
			}
			else
			{
				pan.depth = sceneDepth + 1;
				pan.transform.localScale = Vector3.one * 2f;
			}
			pan.alpha = 0f;
			pan.gameObject.SetActive (isActive);
		}

		void SetPanelShowed(UIPanel pan)
		{
			pnlCurrentScene.alpha = 1f;
			pnlCurrentScene.transform.localScale = Vector3.one;
			pnlCurrentScene.depth = sceneDepth;
			pan.gameObject.SetActive (true);
		}
	}
}