﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Core.Global
{
	public class AudioSystem : MonoBehaviour // === СИСТЕМА ВОСПРОИЗВЕДЕНИЯ ФОНОВОЙ МУЗЫКИ И ОТДЕЛЬНЫХ ЗВУКОВ === //
	{
		// Перечень используемых звуков +++++++++++++++++
		public AudioClip bgMain;
		public AudioClip fxClick;
		// Перечень используемых звуков -----------------

		public static void PlayFx(AudioClip c) // ============================ Запуск звукового эффекта
		{
			AudioSource s = sndEffectFree;
			s.clip = c;
			s.Play ();
		}
		
		public static void PlayBg(AudioClip c, float v) // ============================ Смена фоновой музыки
		{
			if (currentBg == null || currentBg.clip != c)
			{
				Sound snd = new Sound ();
				snd.clip = c;
				snd.volume = v;
				newBg = snd;
			}
		}

		public class Sound // === Класс для фоновой музыки
		{
			public AudioClip clip = null;
			public float volume = 1f;
		}

		// ========================================================================================== ССылки на объекты и компоненты ================================== +++
		static GCore_Audio instValue;
		public static GCore_Audio inst
		{
			get
			{
				if (instValue == null) instValue = GameObject.Find("/UI Root").GetComponent<GCore_Audio> (); // гораздо быстрее чем FindObjectsOfType // Используется точный путь
				return instValue;
			}
		}

		static GameObject objAudioMainValue;
		public static GameObject objAudioMain
		{
			get
			{
				if (objAudioMainValue == null) 
				{
					objAudioMainValue = NGUITools.AddChild(inst.gameObject);
					objAudioMainValue.name = "AudioMain";
				}
				return objAudioMainValue;
			}
		}

		static AudioSource sndAudioMainValue;
		public static AudioSource sndAudioMain
		{
			get
			{
				if (sndAudioMainValue == null) 
				{
					sndAudioMainValue = objAudioMain.AddMissingComponent<AudioSource> ();
					sndAudioMainValue.loop = true;
				}
				return sndAudioMainValue;
			}
		}

		static List<AudioSource> listEffects = new List<AudioSource>();
		static int lastPos = 0;

		static AudioSource sndEffectFree
		{
			get
			{
				if (listEffects.Count != 0) foreach (AudioSource item in listEffects) if (! item.isPlaying)
				{
					return item;
				}

				lastPos = 0;
				AudioSource newS = NGUITools.AddChild(objAudioMain).AddComponent<AudioSource> ();
				newS.loop = false;
				newS.volume = Mathf.Pow(volumeEffects,2f);
				listEffects.Add(newS);
				return newS;
			}
		}
		// ========================================================================================== ССылки на объекты и компоненты ================================== ---

		// ========================================================================================== Регулировка громкости звуков ================================== +++
		static float volumeEffectsValue = 1f;
		public static float volumeEffects
		{
			get
			{
				return volumeEffectsValue;
			}
			set
			{
				volumeEffectsValue = value;
				foreach (AudioSource item in listEffects)
				{
					item.volume = Mathf.Pow(volumeEffectsValue,2f);
				}
				PlayerPrefs.SetFloat("GCore_Audio_volumeEffects",volumeEffectsValue);
			}
		}
		// ========================================================================================== Регулировка громкости звуков ================================== ---

		// ========================================================================================== Регулировка громкости фоновой музыки ================================== +++
		static float volumeBgValue = 1f;
		public static float volumeBg
		{
			get
			{
				return volumeBgValue;
			}
			set
			{
				volumeBgValue = value;
				PlayerPrefs.SetFloat("GCore_Audio_volumeBg",volumeBgValue);
			}
		}
		// ========================================================================================== Регулировка громкости фоновой музыки ================================== ---

		static Sound currentBg = null;
		static Sound newBg = null;
		float volumeBgSound = 1f; // Ограничение максимальной громкости конкретной фоновой музыки
		float volumeBgChanging = 0f; // Переменная для плавного затухания / нарастания громкости при смене фоновой музыки

		float currentVolume = 0f;
		float newVolume = 0f;
		void Update()
		{
			// ==================================== Смена фоновой музыки и обновление громкости ======================= +++
			if (currentBg == null)
			{
				volumeBgChanging = 0f;
				if (newBg != null)
				{
					currentBg = newBg;
					sndAudioMain.clip = currentBg.clip;
					sndAudioMain.Play();
					volumeBgSound = currentBg.volume;
				}
			}
			else if (newBg != null && currentBg != newBg)
			{
				volumeBgChanging -= 1f * Time.deltaTime;
				if (volumeBgChanging <= 0f)
				{
					volumeBgChanging = 0f;
					currentBg = newBg;
					sndAudioMain.clip = currentBg.clip;
					volumeBgSound = currentBg.volume;
				}
			}
			else
			{
				if (volumeBgChanging < 1f)
				{
					volumeBgChanging += 1f * Time.deltaTime;
					if (volumeBgChanging > 1f) volumeBgChanging = 1f;
				}
			}

			newVolume = volumeBgSound * volumeBgChanging * volumeBg;
			if (newVolume != currentVolume)
			{
				currentVolume = newVolume;
				sndAudioMain.volume = Mathf.Pow(currentVolume,2f);
			}
			// ==================================== Смена фоновой музыки и обновление громкости ======================= ---
		}

		public static bool isAwaked = false;
		void Awake() // === Загружаем громкость звуков и фоновой музыки
		{
			gameObject.AddMissingComponent<AudioListener> ();
			volumeEffects = PlayerPrefs.GetFloat("GCore_Audio_volumeEffects",1f); // Загрзка громкости звуков
			volumeBg = PlayerPrefs.GetFloat("GCore_Audio_volumeBg",1f); // Загрзка громкости фоновой музыки
			isAwaked = true;
		}
	}
}