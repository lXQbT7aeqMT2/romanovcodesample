﻿using UnityEngine;
using System.Collections;
using Core.Global;
using Locals;
using UI.Battle;
using Memory;
using Core.Consts;

namespace UI
{
	public class UICardInformation : MonoBehaviour // === ПРИМЕР ОРГАНИЗАЦИИ ОТДЕЛЬНОГО ИНТЕРФЕЙСНОГО ОКНА === //
	{
		static float animTimer = 0.2f;

		// ======================================== Ссылка на текущий компонент =========================================================
		static UICardInformation instValue;
		public static UICardInformation inst
		{
			get
			{
				if (instValue == null) instValue = GameObject.Find("/UI Root/Camera/CardInformation").GetComponent<UICardInformation> ();
				return instValue;
			}
		}

		public GameObject anchor;
		public GameObject blocker;

		public UI2DSprite Ico;
		public UILabel cardName;
		public UILabel cardLevel;
		public UILabel labelCost;
		public UILabel labelHealth;
		public UILabel labelAttack;

		// ================================================================ ОКНО таланта и способности
		public Tools.RUI_WindowOpenClose windowAbility;
		public UI2DSprite sprAbilityIco;
		public UILabel sprAbilityDescr;

		int cardInfoId = -1;
		public void InitInfo(Vector3 pos, CardComponent cardInfo)
		{
			anchor.SetActive(true);
			blocker.SetActive(true);

			TweenAlpha TA = anchor.AddMissingComponent<TweenAlpha> ();
			TA.from = 0f;
			TA.to = 1f;
			TA.duration = animTimer;
			TA.animationCurve = GCore_CurveCollection.inst.SemiWave;
			SCore_Tweens.PlayTweenForward (TA);
			
			TweenScale TS = anchor.AddMissingComponent<TweenScale> ();
			TS.from = Vector3.zero;
			TS.to = Vector3.one;
			TS.duration = animTimer;
			TS.animationCurve = GCore_CurveCollection.inst.OverSemiWave;
			SCore_Tweens.PlayTweenForward (TS);
			
			TweenPosition TP = anchor.AddMissingComponent<TweenPosition> ();
			TP.from = pos;
			TP.to = Vector3.zero;
			TP.duration = animTimer;
			TP.animationCurve = GCore_CurveCollection.inst.SemiWave;
			SCore_Tweens.PlayTweenForward (TP);

			if (TA.onFinished.Count == 0)
			{
				EventDelegate.Add(TA.onFinished,delegate() {
					if (TA.value <= 0f) 
					{
						anchor.SetActive(false);
						blocker.SetActive(false);
						Ico.sprite2D = null;
					}
				});
			}

			// =================================================================== Заполнение информации о карте
			cardInfoId = cardInfo.cardId;
			cardInfo.cardInfo.LoadBigSprite (Ico);
			cardName.text = cardInfo.cardInfo.cardName;
			cardLevel.text = "Уровень 1";
			labelCost.text = cardInfo.cardInfo.cost.ToString();
			labelHealth.text = cardInfo.cardInfo.health.ToString();
			labelAttack.text = cardInfo.cardInfo.attack.ToString();
		}

		public void HideInfo()
		{
			TweenAlpha TA = anchor.AddMissingComponent<TweenAlpha> ();
			if (TA.value >= 1f)
			{
				SCore_Tweens.PlayTweenReverse (TA);
				TweenScale TS = anchor.AddMissingComponent<TweenScale> ();
				SCore_Tweens.PlayTweenReverse (TS);
				TweenPosition TP = anchor.AddMissingComponent<TweenPosition> ();
				SCore_Tweens.PlayTweenReverse (TP);
			}
		}

		void Awake()
		{
			anchor.SetActive(false);
			blocker.SetActive(false);
		}

		float unloadTimer = 10f;
		void Update()
		{
			if (blocker.activeSelf)
			{
				labelCost.transform.localPosition = Vector3.right * (float)(labelCost.transform.parent.GetComponent<UIWidget> ().width + 15);
				labelHealth.transform.localPosition = Vector3.right * (float)(labelHealth.transform.parent.GetComponent<UIWidget> ().width + 15);
				labelAttack.transform.localPosition = Vector3.right * (float)(labelAttack.transform.parent.GetComponent<UIWidget> ().width + 15);
			}

			if (unloadTimer <= 0f)
			{
				unloadTimer = 10f;
				Resources.UnloadUnusedAssets();
				Debug.Log("Unused sprites unloaded");
			}
			else unloadTimer -= Time.deltaTime;
		}

		public void ShowTalantInfo()
		{
			sprAbilityIco.sprite2D = SpriteCollection.inst.sprTalants[(int)SConst_Cards.cardsList[cardInfoId].CAT - 1];
			string txt = LocalizationCore.GetValue ("talant_name_" + ((int)SConst_Cards.cardsList [cardInfoId].CAT).ToString ());
			txt += "\n\n";
			txt += LocalizationCore.GetValue ("talant_description_" + ((int)SConst_Cards.cardsList [cardInfoId].CAT).ToString ());
			sprAbilityDescr.text = txt;
			windowAbility.ShowWindow ();
		}

		public void ShowSkillInfo()
		{
			sprAbilityIco.sprite2D = SpriteCollection.inst.sprTalants[(int)SConst_Cards.cardsList[cardInfoId].CST - 1];
			string txt = LocalizationCore.GetValue ("skill_name_" + ((int)SConst_Cards.cardsList [cardInfoId].CST).ToString ());
			txt += "\n\n";
			txt += LocalizationCore.GetValue ("skill_cost") + ": " + SConst_Cards.skillCost[SConst_Cards.cardsList[cardInfoId].CST].ToString();
			txt += "\n\n";
			txt += LocalizationCore.GetValue ("skill_description_" + ((int)SConst_Cards.cardsList [cardInfoId].CST).ToString ());
			sprAbilityDescr.text = txt;
			windowAbility.ShowWindow ();
		}
	}
}